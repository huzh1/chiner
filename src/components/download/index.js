import JSZip from 'jszip';

const DownloadByBlob = (blob, name) => {
  const a = document.createElement('a');
  const url = URL.createObjectURL(blob);
  a.download = name;
  a.href = url;
  a.click();
};

const DownloadZip = (dataList, type, name) => {
  const zip = new JSZip();
  const zipFolder = zip.folder(name);
  const promises = [];
  dataList.forEach((item) => {
     const blob = new Blob([item.content], {type});
     zipFolder?.file(`${item.exportName}`, blob, { binary: true });
  });
  Promise.all(promises).then(() => {
    zip.generateAsync({ type: 'blob' }).then((content) => {
      DownloadByBlob(content,name);
    }).catch(() => {
    });
  });
};

const Download = (data, type, name) => {
  const blob = new Blob(data, {type});
  const a = document.createElement('a');
  const url = URL.createObjectURL(blob);
  a.download = name;
  a.href = url;
  a.click();
};

const DownloadUrl = (url, name) => {
  const a = document.createElement('a');
  a.download = name;
  a.href = url;
  a.click();
};

export {
  Download,
  DownloadUrl,
  DownloadZip,
};
